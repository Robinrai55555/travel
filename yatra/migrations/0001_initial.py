# Generated by Django 2.2.2 on 2019-07-24 22:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Destination',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('images', models.ImageField(upload_to='destination')),
                ('descriptions', models.TextField()),
                ('cost', models.DecimalField(decimal_places=3, max_digits=20)),
                ('country', models.CharField(max_length=200)),
            ],
        ),
    ]
