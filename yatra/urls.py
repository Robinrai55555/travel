from django.urls import path
from .views import*

app_name="yatra"

urlpatterns = [
	path('', Home.as_view(), name="home"),
	path('contact/', ContactView.as_view(), name="contact"),
	path('galery/', GaleryView.as_view(), name="galery"),
]