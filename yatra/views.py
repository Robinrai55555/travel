from django.shortcuts import render
from django.views.generic import*
from .models import*
# Create your views here.


class Home(TemplateView):
    template_name = "home.html"

    def get_context_data(self,**kwrags):
    	context=super().get_context_data(**kwrags)
    	context['yatra']=Destination.objects.all()

    	return context


class ContactView(TemplateView):
    template_name = "contact.html"


class GaleryView(TemplateView):
    template_name = "galery.html"
