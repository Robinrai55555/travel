from django.db import models

# Create your models here.


class Destination(models.Model):
    name = models.CharField(max_length=200)
    images = models.ImageField(upload_to='destination')
    descriptions = models.TextField()
    cost = models.DecimalField(max_digits=20, decimal_places=3)
    country = models.CharField(max_length=200)

    def __str__(self):
        return self.name
